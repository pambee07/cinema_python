def main():
    # Création des tableaux pour stocker les informations des employés
    employeeNames = ["Alice Dupont", "Bob Martin", "Charlie Besson", "Diane Loriot", "Eva Joly"]
    hoursWorked = [35, 38, 35, 38, 40]
    hourlyRates = [12.5, 15.0, 13.5, 14.5, 13.0]
    positions = ["Caissier", "Projectionniste", "Caissier", "Manager", "Caissier"]

    # Affichage des données pour vérification
    print("Nom \t\t\t Heures travaillées \t Taux horaire \t Poste")
    for name, hours, rate, position in zip(employeeNames, hoursWorked, hourlyRates, positions):
        print(f"{name} \t\t {hours} \t\t\t {rate} \t\t {position}")
    

    # Calcul et affichage du salaire hebdomadaire pour chaque employé
    print("Salaire hebdomadaire de chaque employé :")
    for name, hours, rate in zip(employeeNames, hoursWorked, hourlyRates):
        weekly_salary = calculate_weekly_salary(hours, rate)
        print(f"{name} : {weekly_salary} €")

    # Chaîne de caractères searchPosition
    search_position = "Caissier"

    # Liste des employés dont le poste correspond à search_position
    print("\nEmployés dont le poste correspond à", search_position, ":")
    employee_found = False
    for name, position in zip(employeeNames, positions):
        if position == search_position:
            print(name)
            employee_found = True
    if not employee_found:
        print("Aucun employé trouvé.")

def calculate_weekly_salary(hours_worked, hourly_rate):
    if hours_worked > 35:
        return 35 * hourly_rate + (hours_worked - 35) * 1.5 * hourly_rate
    else:
        return hours_worked * hourly_rate

if __name__ == "__main__":
    main()

